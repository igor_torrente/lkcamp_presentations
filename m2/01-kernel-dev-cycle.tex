\section{Brief summary}
\begin{frame}{\secname}
    \begin{itemize}
        \item The kernel uses a rolling development model
        \begin{itemize}
            \item A new release every 2-3 months (since kernel 2.6)
            \item Thousands of changes every new release
            \item Non-semantic version numbers
        \end{itemize}
        \item Mainline kernel changes must be accepted by Linus Torvalds
    \end{itemize}
\end{frame}

\section{Version Numbering}
\begin{frame}{\secname}
    \begin{itemize}
        \item Until kernel 2.6, a new kernel was release every ~2 years
        \item From kernel 2.6 onward, releases happened every 2-3 months (named 2.6.1, 2.6.2 etc)
        \item At arbitrary points, the version was deemed too large
        \begin{itemize}
            \item 2011-07-21, from 2.6.39 to 3.x
            \item 2015-04-12, from 3.19 to 4.x
            \item 2018-03-03, from 4.20 to 5x
        \end{itemize}
    \end{itemize}

    Note that there's no special meaning to the minor version. It's simply increased per-release.
\end{frame}

\section{Development cycle}
\begin{frame}{\secname}
    How can a single person review and accept that many changes?

    \includegraphics[width=1.0\textwidth]{kernel_hierarchy.png}
\end{frame}

\begin{frame}{\secname}
    \begin{itemize}
        \item "Merge window" open for around 2 weeks after a stable release
        \begin{itemize}
            \item Time for accepting new features into the kernel
        \end{itemize}
        \item Kernel stabilizes over the following 6-10 weeks
        \begin{itemize}
            \item Versions released as "-rcN"
            \item E.g., Linux 5.1-rc1
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{\secname}
    Development cycle for 4.16 (all dates in 2018):

    \begin{table}[!b]
        {\carlitoTLF % Use monospaced lining figures
            \begin{tabularx}{
                \textwidth}{Xr}
                \textbf{Release date} & \textbf{Kernel version} \\
                \toprule

                January 28 & 4.15 stable release \\
                February 11 & 4.16-rc1, merge window closes \\
                February 18 & 4.16-rc2 \\
                February 25 & 4.16-rc3 \\
                March 4 & 4.16-rc4 \\
                March 11 & 4.16-rc5 \\
                March 18 & 4.16-rc6 \\
                March 25 & 4.16-rc7 \\
                April 1 & 4.16 stable release \\

                \bottomrule
            \end{tabularx}
        }
    \end{table}
\end{frame}

\section{Kernel maintenance}
\begin{frame}{\secname}
    \begin{itemize}
        \item Kernel development continues while the merge window is closed!
        \item Changes that are to be sent in the next merge window are collected into "-next" trees
        \item Some of those may be accepted into stable releases, given some constraints:
        \begin{itemize}
            \item They must fix a significant bug
            \item They must already be merged into the mainline
        \end{itemize}
        \item These releases get an extra number at the end (e.g., 4.16.1, 4.16.2 etc), usually named something like "4.16.y"
    \end{itemize}
\end{frame}

\begin{frame}{\secname}
    \begin{itemize}
        \item At first, the latest kernel would usually be maintained only until the next stable release
        \item Eventually, the needs of the community brought about Long Term Support (LTS) kernels
        \begin{itemize}
            \item Stable releases maintained for at least 2 years
            \item Somewhat arbitrarily decided on
            \item 4.9.y and 4.14.y were picked based on being the last release of their respective years
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Sending patches}
\begin{frame}{\secname}
    What do maintainers expect from your patch?

    \begin{itemize}
        \item Patches don’t break the build
        \item Their description is well-written
        \item All of them have the proper commit tags
        \item They are easy to understand
        \item They will work on every supported system
        \item They were sent to the correct recipients
        \item Just use plain text in your email
    \end{itemize}
\end{frame}

\begin{frame}{\secname}
    When you contribute to Linux Kernel you must agree with:

    \begin{itemize}
        \item Developer’s Certificate of Origin terms (for sending patches)
        \begin{itemize}
            \item Indicated by a "Signed-off-by" tag
        \end{itemize}
        \item Reviewer’s statement of oversight terms (for reviewers)
        \begin{itemize}
            \item Indicated by a "Reviewed-by" tag
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{\secname}
    \begin{itemize}
        \item Signed-off-by: author and maintainers who contributed to the patch
        \item Co-Developed-by: attribute someone as an author as well
        \item Reviewed-by: reviewed according to the Reviewer’s Statement
        \item Acked-by: maintainer accepted the patch without contributing to it
        \item Tested-by: successfully tested in some environment
        \item Cc: inform that potentially interested person has received this patch
        \item Reported-by: give credit to the bug reporter
        \item Suggested-by: give credit to the person who had the idea
        \item Fixes: commit id which generated the fixed bug
    \end{itemize}
\end{frame}

\defverbatim[colored]\fullPatch{
    \begin{lstlisting}[basicstyle=\tiny]
From: Baolin Wang <>
Subject: [PATCH v3] power: reset: Add Spreadtrum SC27xx PMIC power off support
Date: Fri, 23 Feb 2018 11:32:38 +0800

On Spreadtrum platform, we need power off system through external SC27xx
series PMICs including the SC2720, SC2721, SC2723, SC2730 and SC2731 chips.
Thus this patch adds SC27xx series PMICs power-off support.

Signed-off-by: Baolin Wang <baolin.wang@linaro.org>
---
Changes since v2:
 - Change to build-in this driver.

Changes since v1:
 - Add remove interface.
 - Add regmap checking when probing the driver.
 - Add MODULE_ALIAS()
---
 drivers/power/reset/Kconfig           |    9 +++++
...
---
1.7.9.5
    \end{lstlisting}
}
\begin{frame}{\secname}
    The canonical patch format:

    \fullPatch
\end{frame}


\defverbatim[colored]\patchHeader{
    \begin{lstlisting}[basicstyle=\tiny]
From: Baolin Wang <>
Subject: [PATCH v3] power: reset: Add Spreadtrum SC27xx PMIC power off support
Date: Fri, 23 Feb 2018 11:32:38 +0800
    \end{lstlisting}
}
\begin{frame}{\secname}
    Header:

    \patchHeader
\end{frame}

\defverbatim[colored]\patchSubject{
    \begin{lstlisting}[basicstyle=\tiny]
On Spreadtrum platform, we need power off system through external SC27xx
series PMICs including the SC2720, SC2721, SC2723, SC2730 and SC2731 chips.
Thus this patch adds SC27xx series PMICs power-off support.

Signed-off-by: Baolin Wang <baolin.wang@linaro.org>
---
    \end{lstlisting}
}
\begin{frame}{\secname}
    Summary + Tags:

    \patchSubject
\end{frame}

\defverbatim[colored]\patchChangelog{
    \begin{lstlisting}[basicstyle=\tiny]
Changes since v2:
 - Change to build-in this driver.

Changes since v1:
 - Add remove interface.
 - Add regmap checking when probing the driver.
 - Add MODULE_ALIAS()
---
    \end{lstlisting}
}
\begin{frame}{\secname}
    Patch Changelog since v1:

    \patchChangelog
\end{frame}

\defverbatim[colored]\patchDiff{
    \begin{lstlisting}[basicstyle=\tiny]
 drivers/power/reset/Kconfig           |    9 +++++
...
---
1.7.9.5
    \end{lstlisting}
}
\begin{frame}{\secname}
    Diff + Extras:

    \patchDiff
\end{frame}

\section{References}
\begin{frame}{\secname}
    \begin{itemize}
        \item \url{http://kroah.com/log/blog/2018/02/05/linux-kernel-release-model/}
        \item \url{https://www.kernel.org/doc/html/v4.19/process/2.Process.html\#the-lifecycle-of-a-patch}
        \item \url{https://www.kernel.org/category/releases.html}
        \item \url{https://www.kernel.org/doc/html/latest/process/stable-kernel-rules.html}
        \item \url{https://developercertificate.org/}
        \item \url{https://www.kernel.org/doc/html/v4.17/process/submitting-patches.html}
    \end{itemize}
\end{frame}
