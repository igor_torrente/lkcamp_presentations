# lkcamp_presentations

Slide sources and other materials used to generate the meeting presentations.

The bulk of the presentations are written in LaTeX.
However, it is also possible to write a presentation in reStructuredText, see
the *reStructuredText* section below for more info.

## LaTeX

### Dependencies:
#### Debian installation:
```bash
$ sudo apt install texlive-latex-extra ttf-dejavu texlive-fonts-extra xzdec latexmk
$ tlmgr init-usertree
$ # use an older repository if running Ubuntu 18.04
$ [ "`lsb_release -r | awk '{ print $2 }'`" == "18.04" ] && tlmgr option repository ftp://tug.org/historic/systems/texlive/2017/tlnet-final
$ tlmgr install dsfont carlito
$ updmap --user #update maps
```

### Build
Just calling make will build all presentations. You can also build
just an specific presentation by calling:
```bash
$ make <project-name>
```

## reStructuredText

The *rst2pdf* folder contains the base files used by `rst2pdf` to generate a pdf
presentation from rST source using the LKCAMP theme.

The *rst2pdf/demo* folder contains a demo presentation written in rST, so you
can see what it looks like.
The pdf slide generated from it can be found at
https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/device_drivers-rst2pdf.pdf .

An example of a slide written in rST that was presented in a meeting can be seen
in *extra/rcu*.

Feel free to use it as a base if you want to write a presentation in rST.

### Dependencies

To compile you need rst2pdf and also the carlito font.

#### Archlinux

If you're on ArchLinux, you can get the carlito font from the `ttf-carlito`
package, and rst2pdf from the AUR.

### Build

To build any rST presentation just enter its folder and run `make`.
